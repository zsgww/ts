/* eslint-disable */

let nome: string = 'Junior';
let idade: number = 31;
let adulto: boolean = true;
let simbolo: symbol = Symbol('qualquer-symbolo');

let pessoa: {
  nome: string;
  idade: number;
  adulto?: boolean;
} = {
  nome: 'Junior',
  idade: 31,
};

console.log(pessoa.nome);

function soma(x: number, y: number): number {
  return x + y;
}

const soma2: (x: number, y: number) => number = (x, y) => x + y;
