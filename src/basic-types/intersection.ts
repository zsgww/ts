// & AND

type TemNome = { nome: string };
type TemSobrenome = { sobrenome: string };
type TemIdade = { idade: number };
// type Pessoa = TemNome | TemSobrenome | TemIdade;
type Pessoa = TemNome & TemSobrenome & TemIdade;

const pessoa: Pessoa = {
  nome: 'Junior',
  sobrenome: 'Lima',
  idade: 30,
};

export { pessoa };
