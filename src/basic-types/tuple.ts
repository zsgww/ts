// Array de tamanho e tipo específicos

const dadosCliente: readonly [number, string] = [1, 'Junior'];
const dadosCliente1: [number, string, string] = [1, 'Junior', 'Lima'];
const dadosCliente2: [number, string, string?] = [1, 'Junior'];
const dadosCliente3: [number, string?, ...string[]] = [1, 'Junior', 'Damyres'];

console.log(dadosCliente);
dadosCliente[0] = 100;

console.log(dadosCliente);
console.log(dadosCliente1);
console.log(dadosCliente2);
console.log(dadosCliente3);
