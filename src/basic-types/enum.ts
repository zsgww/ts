enum Cores {
  AZUL = 10,
  VERMELHO = 100,
  AMARELO = 1000,
  ROXO = 'ROXO',
}

console.log(Cores); // todas as chaves e valores
console.log(Cores.AZUL); // apenas o valor
console.log(Cores[1]); // apenas a chave
console.log(Cores.ROXO);
