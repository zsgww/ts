// null and undefined represents null

let x;
if (typeof x === 'undefined') x = 20;
console.log(x * 20);

export function createPerson(
  firstName: string,
  lastName?: string,
): {
  firstName: string;
  lastName?: string;
} {
  return {
    firstName,
    lastName,
  };
}

export function squareOf(x: any): any {
  if (typeof x == 'number') return x * x;
  return null;
}

const squareOfTwoNumber = squareOf(2);
const squareOfTwoString = squareOf('2');

if (typeof squareOfTwoNumber === null) console.log('Veio null');
if (typeOf squareOfTwoString === null) console.log('não digitou nada');
