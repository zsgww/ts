// Generic
const nomes: Array<string> = ['Junior', 'Damyres'];
console.log(nomes);

const numeros: number[] = [1, 2, 3, 4];
console.log(numeros);

export function multiplicaArgs(...args: Array<number>): number {
  return args.reduce((ac, valor) => ac * valor, 1);
}
const resultadoMultiplicacao = multiplicaArgs(2, 4, 6);
console.log(resultadoMultiplicacao);

export function concatenaString(...args: string[]): string {
  return args.reduce((ac, valor) => ac + valor);
}
const concatenacao = concatenaString('a', 'b', 'c');
console.log(concatenacao);
