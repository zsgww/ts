function semRetorno(...args: string[]): void {
  console.log(args.join(' '));
}

semRetorno('Junior', 'Lima');
semRetorno('Damyres', 'Maciel');

const pessoa = {
  nome: 'Junior',
  sobrenome: 'Lima',

  exibirNome(): void {
    console.log(this.nome + ' ' + this.sobrenome);
  },
};

pessoa.exibirNome();

export { pessoa };
