const objetoA: {
  chaveA: string;
  readonly chaveB: string;
  chavec?: string;
  [key: string]: unknown;
} = {
  chaveA: 'valorA',
  chaveB: 'valorB',
};

objetoA.chaveA = 'outro valor';
objetoA.chavec = 'foi criado';
objetoA.chaveD = 'chaveD';

console.log(objetoA);
